<?php get_header(); ?>
<style>
.form-allowed-tags{font-size:10px!important; color:#999!important;}

.comment-reply-title{	color: #00948e; font-size: 36px!important; font-family:'lavanderia_sturdy';}
textarea{width:100%}
#input {
  vertical-align: top;
  font-size: 18px;
  font-weight: normal;
  font-family: 'foundrysterling-boldregular';
  color: #fff;
  padding: 0 7px;
  margin-right: 14px;
  line-height: 28px;
  height: 28px;
  -ms-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  max-width: 100%;
  background:#00948e;
  border:0px;
}
</style> 
    <div id="background_image"></div>

    <!-- section -->
    <section role="main" id="blog_content">
        <div id="blog_left">        
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    
                <!-- article -->
                <article class="stash-post">
                    <div class="post-meta">
                        <h2><?php the_date(); ?>, <?php the_author(); ?></h2>
                    </div>
                    <div class="post-title">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="post-content">
                        <?php the_content(); ?>
                    </div>
                      <?php comments_template( '', true ); ?>
		

                        <br class="clear">
                </article>
                <!-- /article -->
                
            <?php endwhile; ?>

            <?php else: ?>
    
                <!-- article -->
                <article>
                    
                    <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                    
                </article>
                <!-- /article -->
            
            <?php endif; ?>
        </div>
    
        <div id="blog_right">
            <article id="sidebar_social_media">
                <?php echo get_social_links(); ?>
            </article>

            <?php echo get_blog_collections(); ?>

            <div id="wp_sidebar">
                <?php get_search_form(); ?>

                <!-- Mailing List -->
                <article class="mailing-list">
                    <div>
                        <h1>Get creative</h1>
                        <h2>Join The Community</h2>
                        <?php echo mailchimp_signup(); ?>
                    </div>
                </article>

                <?php get_sidebar(); ?>
            </div>

        </div>
           </section>
    
<?php // get_sidebar(); ?>


<?php get_footer(); ?>