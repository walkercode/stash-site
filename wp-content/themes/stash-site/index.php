<?php get_header(); ?>
    <div id="background_image" class="blog-background-image"></div>

    <section id="blog_header">
        <article id="blog_header_title">
            <div>
                <h1>Take a break!</h1>
                <h2>Welcome to Your Inspired Knitting Community</h2>
            </div>
        </article>
        <hr>
        <article id="blog_mailing_list" class="mailing-list">
            <div>
                <h1>Dreaming in yarn?</h1>
                <h2>Unravel with us.</h2>
                <?php echo mailchimp_signup(); ?>
            </div>
        </article>
    </section>

    <!-- section -->
    <section role="main" id="blog_content">
        <div id="blog_left">
            <?php get_template_part('loop'); ?>
            
            <?php get_template_part('pagination'); ?>
        </div>
    

        <div id="blog_right">
            <article id="sidebar_social_media">
                <?php echo get_social_links(); ?>
            </article>

            <?php echo get_blog_collections(); ?>

            <div id="wp_sidebar">
                <?php get_search_form(); ?>

                <!-- Mailing List -->
                <article class="mailing-list">
                    <div>
                        <h1>Get creative</h1>
                        <h2>Join The Community</h2>
                        <?php echo mailchimp_signup(); ?>
                    </div>
                </article>

                <?php get_sidebar(); ?>
            </div>

        </div>
        
    </section>

<?php get_footer(); ?>