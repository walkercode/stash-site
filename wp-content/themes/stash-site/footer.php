            <section id="footer_mailing_list">
                <article class="mailing-list">
                    <div>
                        <h1>Knit Inspired</h1>
                        <h2>Get Weekly Updates</h2>
                        <?php echo mailchimp_signup(); ?>
                    </div>
                </article>
            </section>
            <section id="stash_footer">
                <div id="stash_footer_nav">
                    <nav class="nav" role="navigation">
                        <?php html5blank_nav(); ?>
                    </nav>
                    <?php echo get_social_links(); ?>
                </div>
                <div id="stash_footer_address">
                    <address>
                        <p>
                            110 SW 3rd Street <br>
                            Corvallis, Oregon 97330 <br>
                            (541) 753-9276
                        </p>
                        <a class="footer-logo" href="<?php echo home_url(); ?>" title="Click for the Stash Home Page">
                            <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                            <img src="<?php echo get_template_directory_uri(); ?>/stash-img/global/logo.white.png" alt="Home Link Logo" class="logo-img">
                        </a>
                    </address>
                </div>
            </section>

            <!-- footer -->
            <!-- <footer class="footer" role="contentinfo"> -->
                
                <!-- copyright -->
                <!-- <p class="copyright"> -->
                    <!-- &copy; <?php echo date("Y"); ?> Copyright <?php bloginfo('name'); ?>. <?php _e('Powered by', 'html5blank'); ?>  -->
                    <!-- <a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>. -->
                <!-- </p> -->
                <!-- /copyright -->
                
            <!-- </footer> -->
            <!-- /footer -->
        
        </div>
        <!-- /wrapper -->

        <?php wp_footer(); ?>
        
        <!-- analytics -->
        <script type="text/javascript">
            $(".tribe-events-single .cart").submit(function() {
                alert("Now that you have chosen a ticket, please click \"add to cart\" and then pay right away. If you take too long to pay, the website will give you errors on the Paypal page. Sorry about that technicality.");
                return true;
            });
        </script>
    
    </body>
</html>