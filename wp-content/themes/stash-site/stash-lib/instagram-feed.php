<?php

/**
 * gets all instagram photos from IG
 *
 * requires token and user id
 * 
 * 1. To retrieve your access token and user ID, go here:
 * https://api.instagram.com/oauth/authorize/?client_id=ab103e54c54747ada9e807137db52d77&redirect_uri=http://blueprintinteractive.com/tutorials/instagram/uri.php&response_type=code
 * (it's ok)
 *
 * 2. use code from: http://www.blueprintinteractive.com/blog/how-instagram-api-fancybox-simplified
 *    
 * @return string - the html
 */
function get_instagram_feed()
{
    // $token = '6536723.ab103e5.edbbcfdf27794afeba5f9b4aa0064c0a';
    // $user_id = '6536723';
    $token = '469990376.ab103e5.b9bc8e85b7054ee993992889ab9a808c';
    $user_id = '469990376';

    $url = "https://api.instagram.com/v1/users/{$user_id}/media/recent/?access_token={$token}";

    // this section must have a unique id
    $ig_id = 'igfeed' . rand();

    // this file caches results, there is only one url
    // it ever hits, so we only need one file
    $cache = 'instagram-cache.json';

    // we started with an empty string, and now we're here.
    $html = "";

    if(file_exists($cache) AND filemtime($cache) > time() - 60*30)
    {
        // If a cache file exists, and it is newer than 10 minutes, use it
        $response = file_get_contents($cache); //Decode as an json array
    }
    else
    {
        // Make an API request and create the cache file
        // For example, gets the 32 most popular images on Instagram

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        curl_close($ch); 

        $images = array();

        if($response)
        {
            // Save the json
            file_put_contents($cache, $response); 
        }
    }

    $result = json_decode($response);

    foreach ($result->data as $ig)
    {
        $caption = str_replace('"', '\"', $ig->caption->text);
        // quick way to shorten caption
        // choose first six words
        $words = explode(' ', $ig->caption->text);
        $ig->caption->text = implode(" ", array_splice($words, 0, 6));

        // Do something with this data.
        $html .= "
            <div class=\"instagram-feed-post\">
                <div class=\"instagram-feed-image\">
                    <a class=\"ig_feed\"rel=\"group_{$ig_id}\" href=\"{$ig->images->standard_resolution->url}\" title=\"{$caption}\">
                        <img alt=\"{$caption}\" src=\"{$ig->images->thumbnail->url}\"/>
                    </a>
                </div>
                <div class=\"instagram-feed-caption\">
                    <p>{$ig->caption->text}...</p>
                </div>
            </div>
        ";
    }

    return $html;
}
// end ig feed


function respond($success = TRUE, $message = 'You have been successfully added to the list. Please check your email inbox to confirm the sign up.')
{
    die(json_encode(array(
        'success' => $success,
        'message' => $message,
    )));
}


// BEGIN MAGIC
// If being called via ajax, autorun the function
echo get_instagram_feed();
