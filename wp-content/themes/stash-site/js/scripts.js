// DOM Ready
$(function() {

    var $topMenu = $('header .nav .menu>ul');
    var $topMenuItems = $topMenu.find('li');
    $topMenuItems.slice(0, Math.floor($topMenuItems.length/2)).addClass('left');
    
    // reverse order of the menu items which were not moved to the left
    var items = $topMenu.find('li:not(.left)');
    items.each(function(i, li) {
       $topMenu.prepend(li);
    });


    // SVG fallback
    // toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script#update
    if (!Modernizr.svg) {
        var imgs = document.getElementsByTagName('img');
        var dotSVG = /.*\.svg$/;
        for (var i = 0; i != imgs.length; ++i) {
            if(imgs[i].src.match(dotSVG)) {
                imgs[i].src = imgs[i].src.slice(0, -3) + "png";
            }
        }
    }

});